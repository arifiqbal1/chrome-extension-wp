<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>step 2 - Facebook Friends Organizer</title>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/chrome-app/css/welcome.css">
    <!--<script src="jquery-3.1.1.min.js"></script>-->
</head>
<body>
    <div class="wrapper">
        <div class="content">
            <p>
                Please login to the extension using your username and password. Please find the extension icon <img id="extension-icon-hint" src="<?php echo get_template_directory_uri(); ?>/assets/chrome-app/images/ffo-browser-icon-19.png" > in the top right corner of your browser window
                <img id="icon-pointer" src="<?php echo get_template_directory_uri(); ?>/assets/chrome-app/images/arrow-pointing-top-right.png" class="logo">
            </p>
        </div>
    </div>
</body>
</html>