<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */

/**
 * Twenty Seventeen only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function twentyseventeen_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'twentyseventeen' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'twentyseventeen' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'twentyseventeen-featured-image', 2000, 1200, true );

	add_image_size( 'twentyseventeen-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'twentyseventeen' ),
		'social' => __( 'Social Links Menu', 'twentyseventeen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'assets/css/editor-style.css', twentyseventeen_fonts_url() ) );

	// Define and register starter content to showcase the theme on new sites.
	$starter_content = array(
		'widgets' => array(
			// Place three core-defined widgets in the sidebar area.
			'sidebar-1' => array(
				'text_business_info',
				'search',
				'text_about',
			),

			// Add the core-defined business info widget to the footer 1 area.
			'sidebar-2' => array(
				'text_business_info',
			),

			// Put two core-defined widgets in the footer 2 area.
			'sidebar-3' => array(
				'text_about',
				'search',
			),
		),

		// Specify the core-defined pages to create and add custom thumbnails to some of them.
		'posts' => array(
			'home',
			'about' => array(
				'thumbnail' => '{{image-sandwich}}',
			),
			'contact' => array(
				'thumbnail' => '{{image-espresso}}',
			),
			'blog' => array(
				'thumbnail' => '{{image-coffee}}',
			),
			'homepage-section' => array(
				'thumbnail' => '{{image-espresso}}',
			),
		),

		// Create the custom image attachments used as post thumbnails for pages.
		'attachments' => array(
			'image-espresso' => array(
				'post_title' => _x( 'Espresso', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/espresso.jpg', // URL relative to the template directory.
			),
			'image-sandwich' => array(
				'post_title' => _x( 'Sandwich', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/sandwich.jpg',
			),
			'image-coffee' => array(
				'post_title' => _x( 'Coffee', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/coffee.jpg',
			),
		),

		// Default to a static front page and assign the front and posts pages.
		'options' => array(
			'show_on_front' => 'page',
			'page_on_front' => '{{home}}',
			'page_for_posts' => '{{blog}}',
		),

		// Set the front page section theme mods to the IDs of the core-registered pages.
		'theme_mods' => array(
			'panel_1' => '{{homepage-section}}',
			'panel_2' => '{{about}}',
			'panel_3' => '{{blog}}',
			'panel_4' => '{{contact}}',
		),

		// Set up nav menus for each of the two areas registered in the theme.
		'nav_menus' => array(
			// Assign a menu to the "top" location.
			'top' => array(
				'name' => __( 'Top Menu', 'twentyseventeen' ),
				'items' => array(
					'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
					'page_about',
					'page_blog',
					'page_contact',
				),
			),

			// Assign a menu to the "social" location.
			'social' => array(
				'name' => __( 'Social Links Menu', 'twentyseventeen' ),
				'items' => array(
					'link_yelp',
					'link_facebook',
					'link_twitter',
					'link_instagram',
					'link_email',
				),
			),
		),
	);

	/**
	 * Filters Twenty Seventeen array of starter content.
	 *
	 * @since Twenty Seventeen 1.1
	 *
	 * @param array $starter_content Array of starter content.
	 */
	$starter_content = apply_filters( 'twentyseventeen_starter_content', $starter_content );

	add_theme_support( 'starter-content', $starter_content );
}
add_action( 'after_setup_theme', 'twentyseventeen_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function twentyseventeen_content_width() {

	$content_width = $GLOBALS['content_width'];

	// Get layout.
	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( twentyseventeen_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 740;
	}

	/**
	 * Filter Twenty Seventeen content width of the theme.
	 *
	 * @since Twenty Seventeen 1.0
	 *
	 * @param int $content_width Content width in pixels.
	 */
	$GLOBALS['content_width'] = apply_filters( 'twentyseventeen_content_width', $content_width );
}
add_action( 'template_redirect', 'twentyseventeen_content_width', 0 );

/**
 * Register custom fonts.
 */
function twentyseventeen_fonts_url() {
	$fonts_url = '';

	/*
	 * Translators: If there are characters in your language that are not
	 * supported by Libre Franklin, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$libre_franklin = _x( 'on', 'Libre Franklin font: on or off', 'twentyseventeen' );

	if ( 'off' !== $libre_franklin ) {
		$font_families = array();

		$font_families[] = 'Libre Franklin:300,300i,400,400i,600,600i,800,800i';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function twentyseventeen_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'twentyseventeen-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'twentyseventeen_resource_hints', 10, 2 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twentyseventeen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'twentyseventeen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'twentyseventeen' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'twentyseventeen' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twentyseventeen_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function twentyseventeen_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'twentyseventeen_excerpt_more' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Seventeen 1.0
 */
function twentyseventeen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyseventeen_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function twentyseventeen_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'twentyseventeen_pingback_header' );

/**
 * Display custom color CSS.
 */
function twentyseventeen_colors_css_wrap() {
	if ( 'custom' !== get_theme_mod( 'colorscheme' ) && ! is_customize_preview() ) {
		return;
	}

	require_once( get_parent_theme_file_path( '/inc/color-patterns.php' ) );
	$hue = absint( get_theme_mod( 'colorscheme_hue', 250 ) );
?>
	<style type="text/css" id="custom-theme-colors" <?php if ( is_customize_preview() ) { echo 'data-hue="' . $hue . '"'; } ?>>
		<?php echo twentyseventeen_custom_colors_css(); ?>
	</style>
<?php }
add_action( 'wp_head', 'twentyseventeen_colors_css_wrap' );

/**
 * Enqueue scripts and styles.
 */
function twentyseventeen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentyseventeen-fonts', twentyseventeen_fonts_url(), array(), null );

	// Theme stylesheet.
	wp_enqueue_style( 'twentyseventeen-style', get_stylesheet_uri() );

	// Load the dark colorscheme.
	if ( 'dark' === get_theme_mod( 'colorscheme', 'light' ) || is_customize_preview() ) {
		wp_enqueue_style( 'twentyseventeen-colors-dark', get_theme_file_uri( '/assets/css/colors-dark.css' ), array( 'twentyseventeen-style' ), '1.0' );
	}

	// Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.
	if ( is_customize_preview() ) {
		wp_enqueue_style( 'twentyseventeen-ie9', get_theme_file_uri( '/assets/css/ie9.css' ), array( 'twentyseventeen-style' ), '1.0' );
		wp_style_add_data( 'twentyseventeen-ie9', 'conditional', 'IE 9' );
	}

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentyseventeen-ie8', get_theme_file_uri( '/assets/css/ie8.css' ), array( 'twentyseventeen-style' ), '1.0' );
	wp_style_add_data( 'twentyseventeen-ie8', 'conditional', 'lt IE 9' );

	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'twentyseventeen-skip-link-focus-fix', get_theme_file_uri( '/assets/js/skip-link-focus-fix.js' ), array(), '1.0', true );

	$twentyseventeen_l10n = array(
		'quote'          => twentyseventeen_get_svg( array( 'icon' => 'quote-right' ) ),
	);

	if ( has_nav_menu( 'top' ) ) {
		wp_enqueue_script( 'twentyseventeen-navigation', get_theme_file_uri( '/assets/js/navigation.js' ), array( 'jquery' ), '1.0', true );
		$twentyseventeen_l10n['expand']         = __( 'Expand child menu', 'twentyseventeen' );
		$twentyseventeen_l10n['collapse']       = __( 'Collapse child menu', 'twentyseventeen' );
		$twentyseventeen_l10n['icon']           = twentyseventeen_get_svg( array( 'icon' => 'angle-down', 'fallback' => true ) );
	}

	wp_enqueue_script( 'twentyseventeen-global', get_theme_file_uri( '/assets/js/global.js' ), array( 'jquery' ), '1.0', true );

	wp_enqueue_script( 'jquery-scrollto', get_theme_file_uri( '/assets/js/jquery.scrollTo.js' ), array( 'jquery' ), '2.1.2', true );

	wp_localize_script( 'twentyseventeen-skip-link-focus-fix', 'twentyseventeenScreenReaderText', $twentyseventeen_l10n );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'twentyseventeen_scripts' );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentyseventeen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentyseventeen_content_image_sizes_attr', 10, 2 );

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function twentyseventeen_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'twentyseventeen_header_image_tag', 10, 3 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function twentyseventeen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentyseventeen_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function twentyseventeen_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'twentyseventeen_front_page_template' );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Seventeen 1.4
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function twentyseventeen_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentyseventeen_widget_tag_cloud_args' );

/**
 * Implement the Custom Header feature.
 */
require get_parent_theme_file_path( '/inc/custom-header.php' );

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * Additional features to allow styling of the templates.
 */
require get_parent_theme_file_path( '/inc/template-functions.php' );

/**
 * Customizer additions.
 */
require get_parent_theme_file_path( '/inc/customizer.php' );

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/icon-functions.php' );


/*

 * Chrome extension functions start from here
 * 
 *  */


add_action( 'register_form', 'crf_admin_registration_form' );
function crf_admin_registration_form( $operation ) {
//	if ( 'add-new-user' !== $operation ) {
//		// $operation may also be 'add-existing-user'
//		return;
//	}
	?>

        <input type="hidden" name="user_referrer" value="<?php echo (NULL !== filter_input(INPUT_GET, 'referrer'))?filter_input(INPUT_GET, 'referrer'):''; ?>" />
	<?php
}

add_action( 'user_register', 'crf_user_register' );
function crf_user_register( $user_id ) {
	if ( ! empty( filter_input(INPUT_POST, 'user_referrer') ) ) {
		update_user_meta( $user_id, 'user_referrer', filter_input(INPUT_POST, 'user_referrer') );
	}
}

// Redirect user to instructions page after registration
add_filter( 'registration_redirect', 'ffo_redirect_to_instructions_page' );
add_filter('login_redirect', 'ffo_redirect_to_instructions_page');
function ffo_redirect_to_instructions_page( $registration_redirect ) {
	return home_url('/add-tags-and-notes');
}
// Login page customizations
function ffo_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/chrome-app/images/ffo-icon-128.png);
		height:128px;
		width:128px;
		background-size: 128px 128px;
		background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'ffo_login_logo' );

add_filter('login_message', 'fn_login_message');
function fn_login_message($message){
    $action = filter_input(INPUT_GET, 'action');
    $message = $action == 'register'?'<h1><strong>Step 1 of 2: Create Account</strong></h1>':$message;
    return $message;
}
function ffo_login_logo_url() {
    return '';
}
add_filter( 'login_headerurl', 'ffo_login_logo_url' );

function ffo_login_stylesheet() {
    wp_enqueue_style( 'ffo-custom-login', get_stylesheet_directory_uri() . '/assets/login/css/style-login.css' );
}
add_action( 'login_enqueue_scripts', 'ffo_login_stylesheet' );

add_action('wp_ajax_getSuggestedProfiles', 'getSuggestedProfiles');
add_action('wp_ajax_nopriv_getSuggestedProfiles', 'getSuggestedProfiles');
function getSuggestedProfiles(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'getSuggestedProfiles'){
//        file_put_contents(__DIR__.'/post', print_r(filter_input_array(INPUT_POST),TRUE));
        $user_email = (NULL !== filter_input(INPUT_POST, 'user_email'))?filter_input(INPUT_POST, 'user_email'):'';
        $user = get_user_by( 'email', $user_email );
        global $wpdb;
        
//        file_put_contents(__DIR__.'/user', print_r($user,true));
        
        $tags_count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM fb_tags WHERE wpuser_id = %d", $user->ID) );
        $notes_count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM fb_notes WHERE wpuser_id = %d", $user->ID) );
        $tags_notes_count = ($tags_count || $notes_count)?1:0;
        
//        file_put_contents(__DIR__.'/tags', $tags_count);
        
        /*$sql = "SELECT tags.title from fb_tags AS tags JOIN fb_profiles ON tags.profile_id = fb_profiles.id";
        $result = $wpdb->get_results($sql);*/
        wp_send_json(array('tags_notes_count' => $tags_notes_count, 'user' => $user->display_name));
    }
    wp_die();
}

add_action('wp_ajax_searchByTags', 'searchByTags');
add_action('wp_ajax_nopriv_searchByTags', 'searchByTags');
function searchByTags(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'searchByTags'){
        $search_query = (NULL !== filter_input(INPUT_POST, 'query'))?filter_input(INPUT_POST, 'query'):'';
        $user_id = (NULL !== filter_input(INPUT_POST, 'user_id'))?filter_input(INPUT_POST, 'user_id'):'';
        //file_put_contents(__DIR__.'/post', print_r($_POST,TRUE));
        global $wpdb;
        $sql = "SELECT tags.title, profiles.name, profiles.fbid AS id from fb_tags AS tags JOIN fb_profiles AS profiles ON tags.profile_id = profiles.id WHERE tags.title = '$search_query' AND tags.wpuser_id = $user_id";
        $result = $wpdb->get_results($sql);
        $docs = array();
        foreach ($result as $profile){
            $docs[] = array('name'=>$profile->name, 'tags'=>array(array('tagName'=>$profile->title)), '_id'=>$profile->id);
        }
//        file_put_contents(__DIR__.'/res', print_r($docs,TRUE));
//        echo $wpdb->num_rows > 0? $wpdb->num_rows: 0;
        wp_send_json($docs);
        
    }
}

add_action('wp_ajax_searchFullText', 'searchFullText');
add_action('wp_ajax_nopriv_searchFullText', 'searchFullText');
function searchFullText(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'searchFullText'){
        $search_query = (NULL !== filter_input(INPUT_POST, 'query'))?filter_input(INPUT_POST, 'query'):'';
        $user_id = (NULL !== filter_input(INPUT_POST, 'user_id'))?filter_input(INPUT_POST, 'user_id'):'';
        //file_put_contents(__DIR__.'/post', print_r($_POST,TRUE));
        global $wpdb;
        /*id,name,tagName,noteText*/
        $sql_tags = "SELECT profiles.fbid AS id, profiles.name, tags.title from fb_profiles AS profiles JOIN fb_tags AS tags ON profiles.id = tags.profile_id WHERE tags.wpuser_id = $user_id AND MATCH ( tags.title ) AGAINST ( '\"$search_query\"')";
        $sql_notes = "SELECT profiles.fbid AS id, profiles.name, notes.text from fb_profiles AS profiles JOIN fb_notes AS notes ON profiles.id = notes.profile_id WHERE notes.wpuser_id = $user_id AND MATCH ( notes.text ) AGAINST ( '\"$search_query\"')";
//        file_put_contents(__DIR__.'/sql_fulltext', $sql);
        $result_tags = $wpdb->get_results($sql_tags);
        $result_notes = $wpdb->get_results($sql_notes);
        
        $rows_by_tag = array();
        foreach ($result_tags as $profile){
            $rows_by_tag[] = array('doc' => array('name' => $profile->name, '_id' => $profile->id), 'highlighting' => array('tags.tagName'=>"<strong>$profile->title</strong>") );
        }
        
        $rows_by_notes = array();
        foreach ($result_notes as $profile){
            $rows_by_notes[] = array('doc' => array('name' => $profile->name, '_id' => $profile->id), 'highlighting' => array('notes.noteText'=>str_replace($search_query,"<strong>$search_query</strong>",$profile->text)) );
        }
        
        $rows = array_merge($rows_by_tag,$rows_by_notes);
//        $docs = array();
//        foreach ($result as $profile){
//            $docs[] = array('name'=>$profile->name, 'tags'=>array(array('tagName'=>$profile->title)), '_id'=>$profile->id);
//        }
        
        /*
         *  {
         *   "total_rows":2,
         *   "rows":[
         *           {
         *            "id":"100000649661753",
         *            "score":0.10614608246653923,
         *            "doc":{
         *                   "name":"Kaleem Sajid",
         *                   "tags":[
         *                           {"tagName":"designer","timestamp":"2018-09-17T10:41:15.252Z"},
         *                           {"tagName":"Awesome","timestamp":"2018-09-14T07:09:55.879Z"},
         *                           {"tagName":"Twat","timestamp":"2018-09-14T07:09:41.611Z"}
         *                          ],
         *                   "notes":[
         *                            {"noteText":"Kaleem is a front-end designer and back-end developer.","timestamp":"2018-09-18T05:55:07.560Z"}
         *                           ],
         *                   "_id":"100000649661753",
         *                   "_rev":"4-f0839f478b7957634fa3c0b076d2af5a"
         *                  },
         *            "highlighting":{
    *                                  "tags.tagName":"<strong>designer</strong> Awesome Twat",
    *                                  "notes.noteText":"Kaleem is a front-end <strong>designer</strong> and back-end developer."
    *                                }
         *          },
         *          {
         *           "id":"100000300628247","score":0.02484519974999766,"doc":{"name":"Tauqeer Ijaz Raja","tags":[{"tagName":"Awesome","timestamp":"2018-09-17T10:37:06.629Z"}],"notes":[{"noteText":"Lorem ipsum is simply a dummy text of the printing and typesetting industry. Front end designer at Ilmigo. Lorem ipsum is simply a dummy text of the printing and typesetting industry.","timestamp":"2018-09-17T10:40:23.997Z"}],"_id":"100000300628247","_rev":"3-e1d9c56cdcd49c98a803b0231bee7017"},"highlighting":{"notes.noteText":"Lorem ipsum is simply a dummy text of the printing and typesetting industry. Front end <strong>designer</strong> at Ilmigo. Lorem ipsum is simply a dummy text of the printing and typesetting industry."}}]
         *          }
         * 
         */
        
        
//        file_put_contents(__DIR__.'/fulltext', print_r($result,TRUE));
//        echo $wpdb->num_rows > 0? $wpdb->num_rows: 0;
        wp_send_json($rows);
        
    }
    wp_die();
}

add_action('wp_ajax_searchByName', 'searchByName');
add_action('wp_ajax_nopriv_searchByName', 'searchByName');
function searchByName(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'searchByName'){
        $search_query = (NULL !== filter_input(INPUT_POST, 'query'))?filter_input(INPUT_POST, 'query'):'';
        $user_id = (NULL !== filter_input(INPUT_POST, 'user_id'))?intval(filter_input(INPUT_POST, 'user_id')):'';
        global $wpdb;
        
        $sql_names = $wpdb->prepare( 
                "SELECT `id`, `name`, `fbid` FROM fb_profiles WHERE name LIKE  '%s' and wpuser_id = %d", '%' . $wpdb->esc_like($search_query) . '%', $user_id
        );
        
        $profiles = $wpdb->get_results( $sql_names );
        
        $rows = array();
        
        if($profiles){
            
            foreach($profiles as $profile){
                $tags_count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(`id`) FROM `fb_tags` WHERE `profile_id` = %d AND `wpuser_id` = %d;", intval($profile->id),intval($user_id)));
                $notes_count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(`id`) FROM `fb_notes` WHERE `profile_id` = %d AND `wpuser_id` = %d;", intval($profile->id),intval($user_id)));
                $rows[] = array('name' => $profile->name, '_id' => $profile->fbid, 'profilemeta' => array('tags' => $tags_count, 'notes' => $notes_count));
            }

            wp_send_json_success($rows);
        }
        
    }
    wp_die();
}

add_action('wp_ajax_fnProfileExists', 'fn_profile_exists');
add_action('wp_ajax_nopriv_fnProfileExists', 'fn_profile_exists');
function fn_profile_exists(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'fnProfileExists'){
//        file_put_contents(__DIR__.'/filexist', print_r(filter_input_array(INPUT_POST),TRUE));
        $profile_id = (NULL !== filter_input(INPUT_POST, 'profileId'))?filter_input(INPUT_POST, 'profileId'):0;
        global $wpdb;
        $profile_table_id = $wpdb->get_var( "SELECT `id` FROM `fb_profiles` WHERE fbid = '".$profile_id."'" );
        echo (!empty($profile_table_id))? $profile_table_id: 0;
        wp_die();
        
    }
}

add_action('wp_ajax_fnGetProfileObject', 'fn_get_profile_object');
add_action('wp_ajax_nopriv_fnGetProfileObject', 'fn_get_profile_object');
function fn_get_profile_object(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'fnGetProfileObject'){
//        file_put_contents(__DIR__.'/getProfile', print_r(filter_input_array(INPUT_POST),TRUE));
        $profile_id = (NULL !== filter_input(INPUT_POST, 'profileId'))?filter_input(INPUT_POST, 'profileId'):0;
        $user_id = (NULL !== filter_input(INPUT_POST, 'userId'))?filter_input(INPUT_POST, 'userId'):0;
        global $wpdb;
        $profile_sql = "SELECT `id`, `name` FROM `fb_profiles` WHERE fbid = '".$profile_id."'";
//        file_put_contents(__DIR__.'/sql', $sql);
        $profile_data = $wpdb->get_row( $profile_sql );
//        file_put_contents(__DIR__.'/pd', print_r($profile_data,TRUE));
        
        if(is_null($profile_data)){
            wp_send_json_error(array('_id'=>0));
        }else{
            $tags_sql = "SELECT `title` FROM fb_tags WHERE `wpuser_id` = ".$user_id." AND `profile_id` = ".$profile_data->id;
            $tags_data = $wpdb->get_results($tags_sql,ARRAY_N);
            $tags_array = array();
            if(sizeof($tags_data)){
                foreach ($tags_data as $tag_element){
                    $tags_array[] = $tag_element[0];
                }
            }

            $notes_sql = "SELECT `id`, `text`, `timestamp` FROM fb_notes WHERE `wpuser_id` = ".$user_id." AND `profile_id` = ".$profile_data->id;
            $notes_data = $wpdb->get_results($notes_sql,ARRAY_N);
            $notes_array = array();
            if(sizeof($notes_data)){
                foreach ($notes_data as $note_element){
                    $notes_array[] = (object) array('id' => $note_element[0], 'noteText'=>$note_element[1], 'timestamp'=>$note_element[2]);//$note_element[0];
                }
            }
            wp_send_json_success(array('name'=>$profile_data->name, 'tags'=>$tags_array, 'notes'=>$notes_array, '_id'=>$profile_id));
        }
    }
}

add_action('wp_ajax_fnSaveTag', 'fn_save_tag');
add_action('wp_ajax_nopriv_fnSaveTag', 'fn_save_tag');
function fn_save_tag(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'fnSaveTag'){
//        file_put_contents(__DIR__.'/savetag', print_r(filter_input_array(INPUT_POST),TRUE));
        $tag_title = (NULL !== filter_input(INPUT_POST, 'tagName'))?filter_input(INPUT_POST, 'tagName'):'';
        $user_id = (NULL !== filter_input(INPUT_POST, 'userId'))?filter_input(INPUT_POST, 'userId'):0;
        $profile_table_id = (NULL !== filter_input(INPUT_POST, 'profileTableId'))?filter_input(INPUT_POST, 'profileTableId'):0;
        global $wpdb;
        $title_exists = $wpdb->get_var( "SELECT `id` FROM `fb_tags` WHERE `title` = '$tag_title' AND `wpuser_id` = ".$user_id." AND profile_id = ".$profile_table_id );
//        file_put_contents(__dir__.'/te', $title_exists);
        if(! empty($title_exists)){
            wp_die();
        }elseif(!empty ($tag_title) && !empty ($user_id) && !empty ($profile_table_id)){
            $sql = "INSERT INTO `fb_tags` VALUES(NULL, '$tag_title', '".date("Y-m-d H:i:s")."', $user_id, $profile_table_id)";
//            file_put_contents(__dir__.'/sql', $sql);
            $inserted = $wpdb->query($sql);
            echo $inserted?$wpdb->insert_id:0;
        }
        wp_die();
        
    }
}

add_action('wp_ajax_fnSaveNote', 'fn_save_note');
add_action('wp_ajax_nopriv_fnSaveNote', 'fn_save_note');
function fn_save_note(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'fnSaveNote'){
//        file_put_contents(__DIR__.'/saveNote', print_r(filter_input_array(INPUT_POST),TRUE));
        $note_text = (NULL !== filter_input(INPUT_POST, 'noteText'))?filter_input(INPUT_POST, 'noteText'):'';
        $user_id = (NULL !== filter_input(INPUT_POST, 'userId'))?filter_input(INPUT_POST, 'userId'):0;
        $profile_table_id = (NULL !== filter_input(INPUT_POST, 'profileTableId'))?filter_input(INPUT_POST, 'profileTableId'):0;
        global $wpdb;
        
        if(!empty ($note_text) && !empty ($user_id) && !empty ($profile_table_id)){
            //$sql = "INSERT INTO `fb_notes` VALUES(NULL, '$note_text', '".date("Y-m-d H:i:s")."', $user_id, $profile_table_id)";
//            file_put_contents(__dir__.'/sql', $sql);
            //$inserted = $wpdb->query($sql);
            $inserted = $wpdb->insert(
                                        'fb_notes',
                                        array(
                                            'text' => $note_text,
                                            'timestamp' => current_time('mysql'),
                                            'wpuser_id' => $user_id,
                                            'profile_id' => $profile_table_id
                                        )
                                );
            $timestamp_sql = "SELECT timestamp FROM `fb_notes` WHERE `id` = ".$wpdb->insert_id;
            $timestamp = $wpdb->get_var($timestamp_sql);
            if($inserted){
                wp_send_json(array('insert_id' => $wpdb->insert_id, 'timestamp' => $timestamp));
            }
        }
        
    }
    wp_die();
}

add_action('wp_ajax_deleteProfileTag', 'delete_profile_tag');
add_action('wp_ajax_nopriv_deleteProfileTag', 'delete_profile_tag');
function delete_profile_tag(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'deleteProfileTag'){
//        file_put_contents(__DIR__.'/deletetag', print_r(filter_input_array(INPUT_POST),TRUE));
        $tag_title = (NULL !== filter_input(INPUT_POST, 'tagName'))?filter_input(INPUT_POST, 'tagName'):'';
        $user_id = (NULL !== filter_input(INPUT_POST, 'userId'))?filter_input(INPUT_POST, 'userId'):0;
        $profile_table_id = (NULL !== filter_input(INPUT_POST, 'profileTableId'))?filter_input(INPUT_POST, 'profileTableId'):0;
        global $wpdb;
        $title_exists = $wpdb->get_var( "SELECT `id` FROM `fb_tags` WHERE `title` = '$tag_title' AND `wpuser_id` = ".$user_id." AND profile_id = ".$profile_table_id );
//        file_put_contents(__dir__.'/te', $title_exists);
        if(is_null($title_exists) ){
            wp_die();
        }elseif(!empty ($tag_title) && !empty ($user_id) && !empty ($profile_table_id)){
            $deleted = $wpdb->delete('fb_tags', array('id'=>$title_exists), array('%d'));
            echo $deleted?$deleted:0;
        }
        wp_die();
        
    }
}

add_action('wp_ajax_deleteProfileNote', 'delete_profile_note');
add_action('wp_ajax_nopriv_deleteProfileNote', 'delete_profile_note');
function delete_profile_note(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'deleteProfileNote'){
//        file_put_contents(__DIR__.'/deleteNote', print_r(filter_input_array(INPUT_POST),TRUE));
        $note_id = (NULL !== filter_input(INPUT_POST, 'noteId'))?filter_input(INPUT_POST, 'noteId'):'';
        global $wpdb;
        $note_id_exists = $wpdb->get_var( "SELECT `timestamp` FROM `fb_notes` WHERE `id` = '$note_id'");
//        file_put_contents(__dir__.'/ne', var_export($note_id_exists,true));
        if( empty($note_id_exists) ){
            wp_die();
        }else{
            $deleted = $wpdb->delete('fb_notes', array('id'=>$note_id), array('%d'));
            echo $deleted?$deleted:0;
        }
        wp_die();
        
    }
}

add_action('wp_ajax_updateProfileNote', 'update_profile_note');
add_action('wp_ajax_nopriv_updateProfileNote', 'update_profile_note');
function update_profile_note(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'updateProfileNote'){
//        file_put_contents(__DIR__.'/updateNote', print_r(filter_input_array(INPUT_POST),TRUE));
        $note_id = (NULL !== filter_input(INPUT_POST, 'noteId'))?filter_input(INPUT_POST, 'noteId'):'';
        $note_text = (NULL !== filter_input(INPUT_POST, 'noteText'))?filter_input(INPUT_POST, 'noteText'):'';
        global $wpdb;
        $note_id_exists = $wpdb->get_var( "SELECT `timestamp` FROM `fb_notes` WHERE `id` = '$note_id'");
//        file_put_contents(__dir__.'/ne', var_export($note_id_exists,true));
        if( empty($note_id_exists) ){
            wp_die();
        }else{
            $updated = $wpdb->update(
                    'fb_notes',
                    array(
                        'text'=>$note_text
                    ),
                    array(
                        'id' => $note_id
                    ),
                    array('%s'),
                    array('%d')
                    );
            echo $updated;
        }
        wp_die();
        
    }
}

add_action('wp_ajax_fnSaveNewProfile', 'fn_save_new_profile');
add_action('wp_ajax_nopriv_fnSaveNewProfile', 'fn_save_new_profile');
function fn_save_new_profile(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'fnSaveNewProfile'){
        //file_put_contents(__DIR__.'/savetag', print_r(filter_input_array(INPUT_POST),TRUE));
        $profile_id = (NULL !== filter_input(INPUT_POST, 'profileId'))?filter_input(INPUT_POST, 'profileId'):0;
        $profile_name = (NULL !== filter_input(INPUT_POST, 'profileName'))?filter_input(INPUT_POST, 'profileName'):'';
        $user_id = (NULL !== filter_input(INPUT_POST, 'userId'))?filter_input(INPUT_POST, 'userId'):'';
        global $wpdb;
        $profile_exists = $wpdb->get_var($wpdb->prepare( "SELECT `id` FROM `fb_profiles` WHERE `fbid` = '%s'" ), $profile_id);
//        file_put_contents(__dir__.'/te', $title_exists);
        if(is_null($profile_exists)){
//            $sql = "INSERT INTO `fb_profiles` VALUES(NULL, '$profile_name', '$profile_id', '".date("Y-m-d H:i:s")."' )";
//            file_put_contents(__dir__.'/sql', $sql);
            
            $inserted = $wpdb->insert('fb_profiles',array('name' => $profile_name, 'fbid' => $profile_id, 'wpuser_id' =>$user_id), array('%s','%s','%d'));
            
//            $inserted = $wpdb->query($sql);
            if($inserted){
                wp_send_json_success(array('_id'=>$wpdb->insert_id));
            }else{
                wp_send_json_error();
            }
        }elseif(!empty ($profile_id)){
            wp_die();
        }
    }
}

add_action('wp_ajax_updateProfileName', 'fn_update_profile_name');
add_action('wp_ajax_nopriv_updateProfileName', 'fn_update_profile_name');
function fn_update_profile_name(){
    header('Access-Control-Allow-Origin: *'); // for any host
    if(NULL !== filter_input(INPUT_POST, 'action') && filter_input(INPUT_POST, 'action') == 'updateProfileName'){
//        file_put_contents(__DIR__.'/updateProfileName', print_r(filter_input_array(INPUT_POST),TRUE));
        $profile_id = (NULL !== filter_input(INPUT_POST, 'profileId'))?filter_input(INPUT_POST, 'profileId'):0;
        $profile_name = (NULL !== filter_input(INPUT_POST, 'profileName'))?filter_input(INPUT_POST, 'profileName'):'';
        global $wpdb;
        $profile_exists = $wpdb->get_var( "SELECT `id` FROM `fb_profiles` WHERE `fbid` = '$profile_id'" );
//        file_put_contents(__dir__.'/te', $title_exists);
        if( empty($profile_exists)){
            wp_die();
        }elseif(!empty ($profile_id) && !empty ($profile_name)){
            $sql = "UPDATE `fb_profiles` SET `name` = '$profile_name' WHERE fbid = '$profile_id'";
//            file_put_contents(__DIR__. '/update_sql', $sql);
            $updated = $wpdb->query($sql);
            if($updated){
                wp_send_json_success(array('rows_affected'=>$updated));
            }else{
                wp_send_json_error();
            }
        }
    }
}

/*add_filter('jwt_auth_expire', 'ffo_jwt_auth_expire');
function ffo_jwt_auth_expire($expiry){
    return 60;
}*/

add_filter('jwt_auth_token_before_dispatch', 'cb_jwt_auth_token_before_dispatch',10,2);
function cb_jwt_auth_token_before_dispatch($data, $user){
    $data['user_id'] = $user->data->ID;
    return $data;
}

function ffo_lost_password_redirect() {
    wp_redirect( '/login-instructions/' ); 
    exit;
}
add_action('after_password_reset', 'ffo_lost_password_redirect');